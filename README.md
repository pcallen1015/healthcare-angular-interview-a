# angular-interview-healthcare
This project contains an Angular healthcare app where you will build a 'log-in' and 'patient profile' page for users to access their health data. 

## Getting Starded

### 1) Downloads

1. Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. Install [Node (and NPM)](https://nodejs.org/en/)

### 2) Clone the Repository

```
git clone https://bitbucket.org/pcallen1015/angular-interview-healthcare-a.git
```

### 3) Run the App
Start the Angular app 

`npm start`

The server should begin running at `localhost:4200`.

----
# Interview Exercise

## The Objective 

The purpose of this exercise is for you to demonstrate your skills working with Angular. The project uses many of the technologies we frequently use to build applications in our team.

## Ground Rules

**Your work should be your own**
You are welcome to use the resources that would normally be at your disposal (Google, Stackoverflow, Documentation, etc.), but your submission should reflect your own problem solving ability.
DON'T, for example, find an existing solution (if one exists), copy+paste, and submit that as your own work, thinking we won't notice. **We will**.

## The Project Structure

The structure of this project is based on Angular-CLI. If you've used this tool before, the structure should be familiar.

There's additional functionality built into the application. **For the sake of simplicity, we'll focus on what you'll be working on:**

```
src/
  |-- app/
	  |-- core/
	  |-- doctor-visits/
	  |-- patients/
		|-- components/
		|-- models/
		|-- services/
		|-- views/
		  |-- patient-profile/
		  |-- patient-profile.component.html
		  |-- patient-profile.component.scss
		  |-- patient-profile.component.spec.ts
		  |-- patient-profile.component.ts
	  |-- vitals/

```

# THE EXERCISE REQUIREMENTS 

## PART 1: The Patient

In this part of the exercise you will build the 'Patient Profile' which is a dashboard of information about the patient. It will be comprised of components that show chunks of information related to a specific patient (e.g. medication, conditions, allergies). 

Here's how it should work...


### 1a. Create a Route to 'Patient Profile' Page

The route should intake a parameter to retrieve a single patient. It is possible this data will be shown somewhere else within the application. 

 
### 1b. Build "Patient Profile" View

In `src/app/patients/views/patient-profile`, implement a view that captures the following general information about a patient:

1. First name
2. Last name
3. Date of Birth
4. Gender
5. Height
6. Weight

See wireframe for reference.


### 1b. Patient Data

Build a component that shows a list of patient's medications. Medication data includes:

1. Medication name (e.g. "Vitamin D")
2. Dosage (e.g. "500 mg")
3. Instructions (e.g. "2x day")

This componenent should have the following capabilities:

1. Title (e.g. "Medication") 
2. Table displaying medication data
3. Collapsible cell view of table 

The client should also have the ability to add, edit, and delete medications. There are two ways you could approach this:

1. If you're also completing the node-interview exercise, you can call the API endpoints from that project and store the new medication in a MongoDB database
2. If you're just focusing on the Angular side of things (or you'd rather just keep them seperate), you can store the medication in an array somewhere.�BUT, you should build it as if you were sending this data to a database.

Hint: There might already be some functionality in the project that could help you here


Now, I would like to replicate this component and show a patient's conditions. 
The component should also have a title, table displaying condition data, and a collapsible cell view. In addition, the client shoud have the ability to add, edit, and delete conditions.

Condition data includes:

1. Condition name (e.g. "Hypertension")
2. Date diagnosed (e.g. "October 12, 2007)
3. Physician (e.g. "Dr. Grey")


## PART 2: Doctor Visits 

In this part of the exercise you will display doctor visit information about a specific patient. Here's how it should work... 


### 2a. Create a Route to 'Medical History' Page

Similar to the patient route, this route should intake a parameter to retrieve a single patient's doctor visit history. It is also possible this data will be shown somewhere else within the application. 


### 2c. Medical History View 

In `src/app/doctor-visits/views/medical-history`, implement a view of a specific patient's entire doctor visit history.

The view should capture all of the following information: 

1. Visit Date
2. Visit Type
3. Physician
4. Location
5. Chief Complaint
6. Procedure
7. Diagnosis
8. Medication
9. Notes


### 2c. Patient Profile View

Returing back to `src/app/patients/views/patient-profile`, implement a snapshot view to display a specific patient's doctor visit history similar to the patient's medication data.

Only the FIVE (5) most recent doctor visits displayed should capture the following information:

1. Visit Date
2. Visit Type 
3. Location
4. Physician 
5. Chief Complaint


## 4. Submission

You're all done!

To submit your solution, send us an [email](mailto:colallen@cisco.com) with the following:

### A. Your Name

... so we know who you are.

### B. A Link to Your Code

Create a repository on a service like Github or Bitbucket and provide the link.

**Please don't commit back into this repository! It will be publically accessible and we don't want people stealing your solution!**

### C. Code Improvements

If you had more time, is there anything you'd do differently (in your code OR in OURS)? What would you do and why?

### D. Reflection & Feedback

What did you think about this exercise? Did you like it? Did you hate it? Were the instructions clear? Was is too easy? Too hard? 
