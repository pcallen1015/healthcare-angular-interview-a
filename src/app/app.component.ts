import { Component, OnInit } from '@angular/core';
import { PatientsService } from './patients/services/patients.service';
import { Patient } from './patients/models/patient';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public loading: boolean = true;
  private patientId = '5b5a0d0d3175883a097b513f';

  constructor(
    private patientsService: PatientsService
  ) { }

  public get currentUser(): Patient { return this.patientsService.currentUser; }

  private login(): void {
    this.loading = true;
    this.patientsService.login(this.patientId).subscribe((patient) => {
      console.debug(patient);
      this.loading = false;
    });
  }

  public ngOnInit(): void {
    this.login();
  }
}

