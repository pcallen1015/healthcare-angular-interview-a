import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

import { PatientsModule } from './patients/patients.module';
import { DoctorVisitsModule } from './doctor-visits/doctor-visits.module';
import { VitalsModule } from './vitals/vitals.module';

import { HeaderComponent } from './core/components/header/header.component';
import { HomePageComponent } from './core/views/home-page/home-page.component';
import { VitalMainComponent } from './vitals/views/vital-main/vital-main.component';

import { ChartModule } from 'angular-highcharts';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'homePage', component: HomePageComponent },
  { path: 'vitals', component: VitalMainComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomePageComponent,
  ],
  imports: [
    PatientsModule,
    DoctorVisitsModule,
    VitalsModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    ChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
