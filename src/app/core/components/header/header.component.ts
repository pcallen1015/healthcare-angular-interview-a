import { Component, OnInit } from '@angular/core';
import { PatientsService } from '../../../patients/services/patients.service';
import { Patient } from '../../../patients/models/patient';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  today: number = Date.now();


  constructor(
    private patientsService: PatientsService,
  ) { }


  public patient: Patient;

  public ngOnInit(): void {
    this.patient = this.patientsService.currentUser;
  }

}
