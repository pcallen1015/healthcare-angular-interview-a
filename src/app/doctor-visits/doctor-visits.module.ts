import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatInputModule, MatFormFieldModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SearchDoctorVisitPipe } from './pipes/search-doctor-visit.pipe';

import { DoctorVisitsService } from './services/doctor-visits.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule, 
  ],
  declarations: [
    SearchDoctorVisitPipe,
  ],
  providers: [DoctorVisitsService]
})

export class DoctorVisitsModule { }
