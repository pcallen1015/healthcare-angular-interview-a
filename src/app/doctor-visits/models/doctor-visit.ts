export class DoctorVisit {
    public _id: string;
    public date: Date;
    public type: String;
    public physician: String;
    public location: String;
    public complaint: String;
    public procedure: String;
    public diagnosis: String;
    public medication: String;
    public note: String;

    constructor(data: any) {
        this._id = data._id;
        this.date = data.date;
        this.type = data.type;
        this.physician = data.physician;
        this.location = data.location;
        this.complaint = data.complaint;
        this.procedure = data.procedure;
        this.diagnosis = data.diagnosis;
        this.medication = data.medication;
        this.note = data.note;
    }
}