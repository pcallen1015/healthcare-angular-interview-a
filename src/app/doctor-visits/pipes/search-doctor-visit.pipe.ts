import { Pipe, PipeTransform } from '@angular/core';
import { DoctorVisit } from '../models/doctor-visit';

@Pipe({
  name: 'searchDoctorVisit'
})
export class SearchDoctorVisitPipe implements PipeTransform {

  transform(items: DoctorVisit[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;

    searchText = searchText.toLowerCase();
    return items.filter(it => {
      return it.type.toLowerCase().includes(searchText) ||
        it.physician.toLowerCase().includes(searchText) ||
        it.location.toLowerCase().includes(searchText) ||
        it.complaint.toLowerCase().includes(searchText) ||
        it.procedure.toLowerCase().includes(searchText) ||
        it.diagnosis.toLowerCase().includes(searchText) ||
        it.medication.toLowerCase().includes(searchText) ||
        it.note.toLowerCase().includes(searchText)
    });

  }

}

