import { TestBed, inject } from '@angular/core/testing';

import { DoctorVisitsService } from './doctor-visits.service';

describe('DoctorVisitsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DoctorVisitsService]
    });
  });

  it('should be created', inject([DoctorVisitsService], (service: DoctorVisitsService) => {
    expect(service).toBeTruthy();
  }));
});
