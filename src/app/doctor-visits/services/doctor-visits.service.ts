import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { DoctorVisit } from '../models/doctor-visit';


@Injectable()
export class DoctorVisitsService {


  constructor(
    private http: HttpClient,
  ) { }

  /**
  * Add new DoctorVisit to current Patient
  */
  addVisit(patientId: string, newVisitData: DoctorVisit): Observable<any> {
    console.debug(newVisitData);
    return this.http.post(`local/patients/${patientId}/visits`, newVisitData).map((data: any[]) => {
      console.debug(data);
      return data.map(item => new DoctorVisit(item));
    });
  }

  /**
   * Delete DoctorVisit from current Patient 
   */
  deleteVisit(patientId: string, doctorVisit: DoctorVisit): Observable<any> {
    return this.http.delete(`local/patients/${patientId}/visits/${doctorVisit._id}`).map((data: any[]) => {
      console.debug(data);
      return data.map(item => new DoctorVisit(item));
    });
  }

}



