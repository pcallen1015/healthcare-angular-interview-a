import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Allergies } from '../../models/patient';

@Component({
  selector: 'app-allergy',
  templateUrl: './allergy.component.html',
  styleUrls: ['./allergy.component.css']
})
export class AllergyComponent implements OnInit {

  @Input() allergy: any[];
  @Output() addEvent: EventEmitter<any> = new EventEmitter();

  public form: Allergies = {
    substance: null,
    reaction: null,
    severity: null
  }
  
  constructor() { }

  ngOnInit() {
  }

  public addAllergy:boolean = false;

  addNewAllergy() {
    this.addAllergy = !this.addAllergy;
  }

  public removeAllergy(index: number) {
    this.allergy.splice(index, 1);
  }

  public saveNew() {
    this.addEvent.emit(Object.assign({}, this.form));
  }


}
