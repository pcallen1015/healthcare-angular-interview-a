import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { familyHistory } from '../../models/patient';

@Component({
  selector: 'app-family-history',
  templateUrl: './family-history.component.html',
  styleUrls: ['./family-history.component.css']
})
export class FamilyHistoryComponent implements OnInit {

  @Input() familyHistory: any[];
  @Output() addEvent: EventEmitter<any> = new EventEmitter();

  public form: familyHistory = {
    familyMember: null,
    diagnosis: null,
    ageOnset: null
  }

  constructor() { }

  ngOnInit() {
  }

  public addFH:boolean = false;
  addNewFH() {
    this.addFH = !this.addFH;
  }

  public removeFH(index: number) {
    this.familyHistory.splice(index, 1);
  }

  public saveNew() {
    this.addEvent.emit(this.form);
  }

}
