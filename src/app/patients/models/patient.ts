import { DoctorVisit } from '../../doctor-visits/models/doctor-visit';
import { Vital } from '../../vitals/models/vital';

export class Condition {
    public name: string;
    public date: Date;
    public physician: string;
}

export class familyHistory {
    public familyMember: string;
    public diagnosis: string;
    public ageOnset: number;
}

export class Allergies {
    public substance: string;
    public reaction: string;
    public severity: string;
}

export class Medication {
    public name: string;
    public dosage: string;
    public instructions: string;
}

export class Patient {
    public _id: string;
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public dob: Date;
    public gender: string;
    public height: string;
    public weight: string;
    public insurance: string;
    public familyHistory: familyHistory[];
    public allergies: Allergies[];
    public chronicCondition: Condition[];
    public medication: Medication[];
    public doctorVisits: DoctorVisit[];
    public vitals: Vital[];

    constructor(data: any) {
        this._id = data._id;
        this.firstName = data.firstName;
        this.middleName = data.middleName;
        this.lastName = data.lastName;
        this.dob = new Date(data.dob);
        this.gender = data.gender;
        this.height = data.height;
        this.weight = data.weight;
        this.insurance = data.insurance;
        this.familyHistory = data.familyHistory;
        this.allergies = data.allergies;
        this.chronicCondition = data.chronicCondition;
        this.medication = data.medication;
        this.doctorVisits = data.doctorVisits || [];
        this.vitals = data.vitals || [];
    }
}
