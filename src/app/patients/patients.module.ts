import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AllergyComponent } from './components/allergy/allergy.component';
import { FamilyHistoryComponent } from './components/family-history/family-history.component';

import { PatientsService } from './services/patients.service';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
  ],
  declarations: [
    AllergyComponent,
    FamilyHistoryComponent,
  ],
  providers: [PatientsService]
})
export class PatientsModule { }
