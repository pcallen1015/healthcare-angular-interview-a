import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Patient } from '../models/patient';


@Injectable()
export class PatientsService {

  private _currentUser: Patient;
  public get currentUser(): Patient { return this._currentUser; }

  constructor(
    private http: HttpClient,
  ) { }

  login(id: string): Observable<any> {
    return this.read(id).map((patient) => {
      this._currentUser = patient;
      return this._currentUser;
    });
  }

  /**
   * Get ALL the Patients
   */
  list(): Observable<any> {
    // HTTP request to the server...
    return this.http.get('/local/patients');
  }

  /**
   * Create a NEW Patient
   */
  create() {
    
  }

  /**
   * Get ONE Patient
   */
  read(id: string): Observable<Patient> {
    return this.http.get(`/local/patients/${id}`).map((data: any) => {
      return new Patient(data);
    });
  }

  /**
   * Update a single Patient
   */
  update(id: string, patient: Patient): Observable<Patient> {
    console.debug(patient);
    return this.http.put(`/local/patients/${id}`, patient).map((data: any) => {
      console.debug(data);
      return new Patient(data);
    });
  }

  /**
   * Delete a single Patient
   */
  delete() {

  }

  

}
