import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BloodPressureGraphComponent } from './blood-pressure-graph.component';

describe('BloodPressureGraphComponent', () => {
  let component: BloodPressureGraphComponent;
  let fixture: ComponentFixture<BloodPressureGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloodPressureGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BloodPressureGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
