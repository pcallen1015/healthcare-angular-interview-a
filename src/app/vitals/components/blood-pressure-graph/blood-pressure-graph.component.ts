import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-blood-pressure-graph',
  templateUrl: './blood-pressure-graph.component.html',
  styleUrls: ['./blood-pressure-graph.component.css']
})
export class BloodPressureGraphComponent implements OnInit {

  @Input('data') bloodPressureData: any[] = [];

  constructor() { }

  ngOnInit() {
    let systolic: any[] = this.bloodPressureData.map((item: any) => [new Date(item.date).getTime(), item.systolic]);
    let diastolic: any[] = this.bloodPressureData.map((item:any) => [new Date(item.date).getTime(), item.diastolic]);

    this.chartConfig.series = [{
      name: 'Systolic',
      color: '#00BCEB',
      data: systolic
    }, {
      name: 'Diastolic',
      color: '#005073',
      data: diastolic
    }];
    this.chart = new Chart(this.chartConfig);
  }

  public chart: Chart;
  chartConfig: any = {
    chart: {
      type: 'line',
      width: 580,
      height: 500,
    },

    title: {
      text: 'BLOOD PRESSURE'
    },

    subtitle: {
      text: 'Systolic vs Diastolic'
    },
    
    yAxis: {
      title: {
        text: 'Pressure (mmHg)'
      },
      minorGridLineWidth: 0,
      gridLineWidth: 0,
      alternateGridColor: null,
      plotBands: [{
        from: 120,
        to: 200,
        color: '#F2FBFD',
        label: {
          text: 'Elevated Systolic',
          style: {
            color: '#606060'
          }
        }
      }, {
        from: 0,
        to: 80,
        color: '#F2FBFD',
        label: {
          text: 'Elevated Diastolic',
          style: {
            color: '#606060'
          }
        }
      }, { 
        from: 80,
        to: 120,
        color: '#FFFFFF',
        label: {
          text: 'Normal',
          style: {
            color: '#606060'
          }
        }
      }]
    },

    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        day: '%d %b %Y' 
     }
    },

    tooltip: {
      valueSuffix: ' mmHg',
      backgroundColor: '#FFFFFF',
      borderRadius: 0,
      crosshairs: true,
      xDateFormat: '%d %b %Y',
      shared: true
    }

    };

  }  

