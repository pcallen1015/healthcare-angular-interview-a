import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CholesterolGraphComponent } from './cholesterol-graph.component';

describe('CholesterolGraphComponent', () => {
  let component: CholesterolGraphComponent;
  let fixture: ComponentFixture<CholesterolGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CholesterolGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CholesterolGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
