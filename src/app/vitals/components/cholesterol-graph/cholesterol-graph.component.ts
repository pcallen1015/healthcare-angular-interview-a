import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-cholesterol-graph',
  templateUrl: './cholesterol-graph.component.html',
  styleUrls: ['./cholesterol-graph.component.css']
})
export class CholesterolGraphComponent implements OnInit {

  @Input('data') cholesterolData: any[] = [];

  constructor() { }

  ngOnInit() {
    let ldl: any[] = this.cholesterolData.map((item: any) => [new Date(item.date).getTime(), item.ldl]);
    let hdl: any[] = this.cholesterolData.map((item: any) => [new Date(item.date).getTime(), item.hdl]);

    this.chartConfig.series = [{
      name: 'HDL',
      color: '#00BCEB',
      data: hdl
    }, {
      name: 'LDL',
      color: '#005073',
      data: ldl
    }];
    this.chart = new Chart(this.chartConfig);
  }


  public chart: Chart;
  chartConfig: any = {
    chart: {
      type: 'column',
      width: 580,
      height: 500

    },
    title: {
      text: 'CHOLESTEROL'
    },

    subtitle: {
      text: 'LDL vs HDL'
    },

    yAxis: {
      allowDecimals: false,
      title: {
        text: 'Units (mg/dL)'
      }
    },

    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        day: '%d %b %Y'
      }
    },

    tooltip: {
      valueSuffix: ' mg/dL',
      backgroundColor: '#FFFFFF',
      borderRadius: 0,
      xDateFormat: '%d %b %Y',
      shared: true
    },

    series: [],
  };
}
