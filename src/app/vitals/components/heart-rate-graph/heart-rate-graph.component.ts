import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-heart-rate-graph',
  templateUrl: './heart-rate-graph.component.html',
  styleUrls: ['./heart-rate-graph.component.css']
})
export class HeartRateGraphComponent implements OnInit {

  @Input('data') heartRateData: any[] = [];

  constructor() { }

  ngOnInit() {
    let bpm: any[] = this.heartRateData.map((item:any) => [new Date(item.date).getTime(), item.bpm]);
    
    this.chartConfig.series = [{
      name: 'Heart Rate',
      color: '#00BCEB',
      data: bpm
    }];
    this.chart = new Chart(this.chartConfig);
  }

  public chart: Chart;
  chartConfig: any = {
    chart: {
      type: 'area',
      width: 580,
      height: 500
    },

    title: {
      text: 'HEART RATE'
    },

    yAxis: {
      title: {
        text: 'Beats per Minute'
      }
    },

    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        day: '%d %b %Y'
      }
    },

    legend: {
      enabled: false,
    },

    tooltip: {
      valueSuffix: ' bpm',
      backgroundColor: '#FFFFFF',
      borderRadius: 0,
      crosshairs: true,
      xDateFormat: '%d %b %Y',
    },
    
    series: [],

  };

}
