import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-weight-graph',
  templateUrl: './weight-graph.component.html',
  styleUrls: ['./weight-graph.component.css']
})
export class WeightGraphComponent implements OnInit {

  @Input("data") weightData: any[] = [];

  constructor() { }

  ngOnInit() {
    let weight: any[] = this.weightData.map((item: any) => [new Date(item.date).getTime(), item.weight]);

    this.chartConfig.series = [{
      name: 'Weight',
      color: '#005073',
      data: weight
    }];
    this.chart = new Chart(this.chartConfig);
  }

  public chart: Chart;
  chartConfig: any = {
    chart: {
      type: 'line',
      width: 580,
      height: 500

    },

    title: {
      text: 'WEIGHT'
    },

    yAxis: {
      title: {
        text: 'Weight (lbs)'
      }
    },

    legend: {
      enabled: false,
    },

    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        day: '%d %b %Y'
      }
    },

    tooltip: {
      valueSuffix: ' lbs',
      backgroundColor: '#FFFFFF',
      borderRadius: 0,
      crosshairs: true,
      xDateFormat: '%d %b %Y',
    },

    series: [],

  };
}
