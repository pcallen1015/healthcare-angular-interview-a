export class bloodPressure {
    public systolic: number;
    public diastolic: number;
    public date: Date; 
}

export class weight { 
    public weight: number;
    public date: Date;
}

export class cholesterol { 
    public ldl: number;
    public hdl: number;
    public date: Date;
}

export class heartRate {
    public bpm: number;
    public date: Date;
}
  
export class Vital { 
    public bloodPressure: bloodPressure[];
    public weight: weight[];
    public cholesterol: cholesterol[];
    public heartRate: heartRate[];

    constructor(data: any) {
        this.bloodPressure = data.bloodPressure;
        this.weight = data.weight;
        this.cholesterol = data.cholesterol;
        this.heartRate = data.heartRate;
    }
}