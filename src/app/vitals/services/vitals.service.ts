import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Vital } from '../models/vital';


@Injectable()
export class VitalsService {

  constructor(
    private http: HttpClient,
  ) { }

  getVital(patientId: string): Observable<any> {
    return this.http.get(`local/patients/${patientId}/vitals`).map((data: any) => {
      console.debug(data);
      return <Vital>data;
    });

  }
}
