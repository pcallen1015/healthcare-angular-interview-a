import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VitalMainComponent } from './vital-main.component';

describe('VitalMainComponent', () => {
  let component: VitalMainComponent;
  let fixture: ComponentFixture<VitalMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VitalMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitalMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
