import { Component, OnInit } from '@angular/core';
import { PatientsService } from '../../../patients/services/patients.service';
import { Vital } from '../../models/vital';

@Component({
  selector: 'app-vital-main',
  templateUrl: './vital-main.component.html',
  styleUrls: ['./vital-main.component.css']
})
export class VitalMainComponent implements OnInit {

  constructor(
    private patientsService: PatientsService
  ) { }

  public vitals: Vital[];

  ngOnInit() {
    this.vitals = this.patientsService.currentUser.vitals;
  }

}
