import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VitalMainComponent } from './views/vital-main/vital-main.component';
import { WeightGraphComponent } from './components/weight-graph/weight-graph.component';
import { CholesterolGraphComponent } from './components/cholesterol-graph/cholesterol-graph.component';
import { BloodPressureGraphComponent } from './components/blood-pressure-graph/blood-pressure-graph.component';
import { HeartRateGraphComponent } from './components/heart-rate-graph/heart-rate-graph.component';

import { MatGridListModule } from '@angular/material/grid-list';
import { ChartModule } from 'angular-highcharts';

import { VitalsService } from './services/vitals.service';

@NgModule({
  imports: [
    CommonModule,
    MatGridListModule,
    ChartModule
  ],
  declarations: [
    VitalMainComponent,
    WeightGraphComponent,
    CholesterolGraphComponent,
    BloodPressureGraphComponent,
    HeartRateGraphComponent
  ],
  providers: [VitalsService],
})
export class VitalsModule { }
